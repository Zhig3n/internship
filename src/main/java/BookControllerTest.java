import com.example.restservice.Book;
import com.example.restservice.BookController;
import com.example.restservice.BookService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.when;

//Class for testing the application
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {BookController.class, BookService.class})
@SpringBootTest
public class BookControllerTest{

    static Book book = new Book("fake book", "fake author", "real category", "fake language", 2020, 123, "fake guid1", "true");
    static Book book2 = new Book("real book", "real author", "fake category", "fake language", 3000, 456, "real guid2", "true");
    static Book book3 = new Book("fake book", "fake author", "fake category", "real language", 2020, 789, "fake guid3", "false");


    @MockBean
    private BookService service;

    @Autowired
    private BookController controler;

    //Preparing service so it doesn't repeat in other methods.
    @Before
    public void prepare(){
        when(service.getAllBooks()).thenReturn(List.of(book2, book, book3));
    }
    //Tests filtering by book name
    @Test
    public void testNameFilter(){

        when(service.filter("Name", "real book")).thenCallRealMethod();

        List<Book> filter = controler.filter("Name", "real book");

        Assert.assertEquals(List.of(book2), filter);
    }
    //Tests filtering by author name
    @Test
    public void testAuthorFilter(){

        when(service.filter("Author", "real author")).thenCallRealMethod();

        List<Book> filter = controler.filter("Author", "real author");

        Assert.assertEquals(List.of(book2), filter);
    }
    //Tests filtering by category
    @Test
    public void testCategoryFilter(){

        when(service.filter("Category", "real category")).thenCallRealMethod();

        List<Book> filter = controler.filter("Category", "real category");

        Assert.assertEquals(List.of(book), filter);
    }
    //Tests filtering by languages
    @Test
    public void testLanguageFilter(){

        when(service.filter("Language", "real language")).thenCallRealMethod();

        List<Book> filter = controler.filter("Language", "real language");

        Assert.assertEquals(List.of(book3), filter);
    }
    //Tests filtering by ISBN code
    @Test
    public void testISBNFilter(){

        when(service.filter("ISBN", "123")).thenCallRealMethod();

        List<Book> filter = controler.filter("ISBN", "123");

        Assert.assertEquals(List.of(book), filter);
    }
    //Tests filtering by book availability
    @Test
    public void testAvailableFilter(){

        when(service.filter("Available", "false")).thenCallRealMethod();

        List<Book> filter = controler.filter("Available", "false");

        Assert.assertEquals(List.of(book3), filter);
    }
    //Tests retrieval of a book by GUID
    @Test
    public void testGetByGuid(){

        when(service.getBookByGUID("real guid2")).thenCallRealMethod();

        List<Book> booklist = controler.getByGUID("real guid2");

        Assert.assertEquals(List.of(book2), booklist);
    }
    //Tests retrieval of info with non existent GUID.
    @Test
    public void testNotGetGuid(){

        when(service.getBookByGUID("non existent guid")).thenCallRealMethod();

        List<Book> booklist = controler.getByGUID("non existent guid");

        Assert.assertEquals(0, booklist.size());
    }
    //Tests book adding to the json file
    @Test
    public void testBookAdding(){
        when(service.addBook(book)).thenCallRealMethod();

        doCallRealMethod().when(service).readIntoList();
        doCallRealMethod().when(service).updateJSON(any(List.class),anyString());

        when(service.getAllBooks()).thenCallRealMethod();

        int sizeBefore = service.getAllBooks().size();
        controler.addBook(book);
        int sizeAfter = service.getAllBooks().size();

        Assert.assertEquals(sizeBefore+1, sizeAfter);
    }
    //Tests book deletion from json file
    @Test
    public void testDeleteBook(){
        when(service.deleteBook("fake guid1")).thenCallRealMethod();

        doCallRealMethod().when(service).readIntoList();
        doCallRealMethod().when(service).updateJSON(any(List.class),anyString());

        when(service.getAllBooks()).thenCallRealMethod();

        when(service.addBook(book)).thenCallRealMethod();

        doCallRealMethod().when(service).readIntoList();

        controler.addBook(book);

        int sizeBefore = service.getAllBooks().size();

        controler.deleteByGuid("fake guid1");

        int sizeAfter = service.getAllBooks().size();


        Assert.assertEquals(sizeBefore-1, sizeAfter);
    }

    //Tests book loaning
    @Test
    public void testBookLoaning(){
        when(service.loanBook(2,"b800b320-31f9-4a7e-bf8b-f18261b31840", 4)).thenCallRealMethod();
        when(service.getAllBooks()).thenCallRealMethod();
        doCallRealMethod().when(service).readIntoList();
        doCallRealMethod().when(service).readUsers();
        doCallRealMethod().when(service).updateJSON(any(List.class),anyString());
        doCallRealMethod().when(service).getBookByGUID("b800b320-31f9-4a7e-bf8b-f18261b31840");

        List <Book> bookLoan = controler.loanBook(2, "b800b320-31f9-4a7e-bf8b-f18261b31840", 4);

        List <Book> controlBook =  controler.getByGUID("b800b320-31f9-4a7e-bf8b-f18261b31840");

        Assert.assertEquals(controlBook.get(0).getAvailable(),  bookLoan.get(0).getAvailable());

    }

}

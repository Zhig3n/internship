package com.example.restservice;

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
public class BookService {

    //Class variables made for easier list and file manipulation
    private List<Book> bookList;
    private List<User> userList;

    //Reads data from json file so it can be used for other functions (filter, deletion, etc.)
    @SuppressWarnings("unchecked")
    public void readIntoList(){
        ObjectMapper mapper = new ObjectMapper();
        try {
            bookList = Arrays.asList(mapper.readValue(Paths.get("BookList.json").toFile(), Book[].class));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    //Reads data from the users.json file. Used for updating loan information.
    public void readUsers(){
        ObjectMapper mapper = new ObjectMapper();
        try {
            userList = Arrays.asList(mapper.readValue(Paths.get("users.json").toFile(), User[].class));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //Updating json file after book deletion and book loan
    public void updateJSON(List updatedList, String FileName){

        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
        try {
            writer.writeValue(new File(FileName), updatedList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Method for adding book
    public Book addBook(Book book){
        readIntoList();
        List <Book> updatedList = new ArrayList<>();
        updatedList.addAll(bookList);
        updatedList.add(book);
        updateJSON(updatedList,"BookList.json");
        return book;
    }

    //Method for loaning books
    public List <Book> loanBook(int id, String guid, int time){
        readUsers();
        readIntoList();
        User userLoan = new User();
        Book loanedBook = new Book();
        int savedInt = 0;

        for(int i = 0; i<userList.size(); i++){
            if(userList.get(i).getId() == id) {
                //Checking if user already has 3 books
                if(userList.get(i).getBorrowed()<3){
                    userLoan = userList.get(i);
                    savedInt = i;
                    break;
                }
                else
                    return null;
            }
        }
        for (int i = 0; i<bookList.size(); i++){
            if(bookList.get(i).getGUID().equals(guid)){
                //Checks if book is available
                if(bookList.get(i).getAvailable().equals("true")){
                    bookList.get(i).setAvailable("false");
                    loanedBook = bookList.get(i);
                    updateJSON(bookList, "BookList.json");
                    break;
                }
                else
                    return null;
            }
        }
        //Checks if loan time is not more than 3 months (12 weeks)
        if(time<=12){
            List<Loan> loan = userLoan.getLoan();
            loan.add(new Loan(guid, time));
            userLoan.setLoan(loan);
            userLoan.setBorrowed(userLoan.getBorrowed()+1);
            userList.set(savedInt, userLoan);
            updateJSON(userList, "users.json");
        }
        else
            return null;
        return Collections.singletonList(loanedBook);
    }

    //Finds and returns book by guid code
    public List<Book> getBookByGUID(String guid){
        return  getAllBooks().stream().filter(b -> b.getGUID().equals(guid)).toList();
    }

    //Filtering of books by names, authors and etc.
    public List<Book> filter(String filterName, String filterValue){
        return switch (filterName) {
            case "Name" -> getAllBooks().stream().filter(book -> book.getName().equals(filterValue)).toList();
            case "Author" -> getAllBooks().stream().filter(book -> book.getAuthor().equals(filterValue)).toList();
            case "Category" -> getAllBooks().stream().filter(book -> book.getCategory().equals(filterValue)).toList();
            case "Language" -> getAllBooks().stream().filter(book -> book.getLanguage().equals(filterValue)).toList();
            case "ISBN" -> getAllBooks().stream().filter(book -> String.valueOf(book.getISBN()).equals(filterValue)).toList();
            default -> getAllBooks().stream().filter(book -> book.getAvailable().equals(filterValue)).toList();
        };
    }

   // Book deletion (by GUID code) from local json file.
   public Book deleteBook(String guid){
        List<Book> newList = new ArrayList<>();
        readIntoList();
        newList.addAll(bookList);
        Book deletedBook = new Book();
        for(int i = 0; i<newList.size(); i++){
            if (newList.get(i).getGUID().equals(guid)){
                deletedBook = newList.get(i);
                newList.remove(i);
            }
        }
        updateJSON(newList, "BookList.json");
        return deletedBook;
    }

    //Method adding for unit testing
    public List<Book> getAllBooks(){
        readIntoList();
        return bookList;
    }
}

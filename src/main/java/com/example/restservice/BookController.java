package com.example.restservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
public class BookController {

    @Autowired
    private BookService bookService;

    //Handles adding books to the local JSON file
    @PostMapping(
            value = "/books/addBook", consumes = "application/json", produces = "application/json")
    public Book addBook(@RequestBody Book book) {
        return bookService.addBook(book);
    }
    //Handles book loaning
    @GetMapping("/books/loanBook")
    public List<Book> loanBook(@RequestParam(value = "id", defaultValue = "") int id, @RequestParam(value = "guid", defaultValue = "") String guid, @RequestParam(value = "time", defaultValue = "") int time){
        return bookService.loanBook(id, guid, time);
    }
    //Handles receiving a specific book by guid
    @GetMapping("/books/guid")
    public List<Book> getByGUID(@RequestParam(value = "guidValue", defaultValue = "") String guid){
        return bookService.getBookByGUID(guid);
    }
    //Handles filtering of books
    @GetMapping("/books/filter")
    public List<Book> filter(  @RequestParam(value = "filterName", defaultValue = "") String filterName, @RequestParam(value = "filterValue", defaultValue = "") String filterValue){
        return bookService.filter(filterName, filterValue);
    }
    //Handles deleting of books from the local JSON file
    @DeleteMapping("/books/{guid}")
    public Book deleteByGuid(@PathVariable ("guid") String guid) {
        return bookService.deleteBook(guid);
    }

}


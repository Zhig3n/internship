package com.example.restservice;

//Class made for the book object
public class Book {

    private String name;
    private String author;
    private String category;
    private String language;
    private int publicationDate;
    private int isbn;
    private String guid;
    private String available;


    //Empty constructor declared on purpose, as without it the JSON file writing wouldn't work properly
    public Book(){

    }
    //Book constructor
    public Book(String name, String author, String category, String language, int publicationDate, int isbn, String guid, String available){
        this.name = name;
        this.author = author;
        this.category = category;
        this.language = language;
        this.publicationDate = publicationDate;
        this.isbn = isbn;
        this.guid = guid;
        this.available = available;
    }
    //Mostly getter methods
    public String getName(){
        return name;
    }
    public String getAuthor(){
        return author;
    }
    public String getCategory(){
        return category;
    }
    public String getLanguage(){
        return language;
    }
    public int getPublicationDate(){
        return publicationDate;
    }
    public String getGUID(){
        return guid;
    }
    public int getISBN(){
        return isbn;
    }
    public String getAvailable(){
        return available;
    }

    public void setAvailable(String availability){
        available = availability;
    }
}

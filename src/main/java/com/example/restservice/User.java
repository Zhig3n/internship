package com.example.restservice;

import java.io.Serializable;
import java.util.List;

//Implements Serializable so I could write lists to the inside of JSON objects in users.json file
public class User implements Serializable {
    private String name;
    private String surname;
    private int borrowed;
    private int id;
    private List<Loan> loan;

    //Empty constructor declared on purpose, as without it the JSON file writing wouldn't work properly
    public User(){

    }
    public User(String name, String surname, int borrowed, int id, List<Loan> loan){
        this.name = name;
        this.surname = surname;
        this.borrowed = borrowed;
        this.id = id;
        this.loan = loan;

    }
    public String getName(){
        return name;
    }
    public String getSurname(){
        return surname;
    }
    public int getBorrowed(){
        return borrowed;
    }
    public int getId(){
        return id;
    }
    public List<Loan> getLoan(){
        return loan;
    }
    public void setLoan(List<Loan> loans){
        loan = loans;
    }
    public void setBorrowed(int borrowed) {
        this.borrowed = borrowed;
    }
}

package com.example.restservice;

import java.io.Serializable;

//Implements Serializable so I could write lists to the inside of JSON objects in users.json file
public class Loan implements Serializable {
    private String guid;
    private int time;

    //Empty constructor declared on purpose, as without it the JSON file writing wouldn't work properly
    public Loan(){

    }
    public Loan(String guid, int time){
        this.guid = guid;
        this.time = time;

    }
    public String getGuid(){
        return guid;
    }
    public int getTime(){
        return time;
    }

}

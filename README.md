# Internship

Some ISBN codes repeat. For example, the book "Norwegian Wood" has an ISBN number, but it never differs as all
off the books are hard paper back and they are not revised or modified in any other way.

There are two json files, one for books called "BookList.json" and the other one is for users with loan information called "users.json".

The project contains 6 java files, 3 of which are for objects such as User, Book and Loan. (Loan is used inside the User object).

There is also a test class to perform tests for the service and controller layers of the project.
